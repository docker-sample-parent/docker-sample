package com.qwfys.docker.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 *
 * @author lwk
 * @version DockerfileMavenSampleApplication, v 0.1 7/29/19 9:03 AM lwk
 */
@SpringBootApplication
public class DockerfileMavenSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerfileMavenSampleApplication.class, args);
    }
}
