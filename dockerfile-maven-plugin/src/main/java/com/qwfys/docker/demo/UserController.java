package com.qwfys.docker.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 *
 * @author lwk
 * @version UserController, v 0.1 7/29/19 9:03 AM lwk
 */
@RestController
public class UserController {

    @GetMapping("/")
    public String home() {
        return "Hello Docker World";
    }
}
