# docker-maven-plugin-sample

## Generate and push docker image to the docker image repository

```bash
mvn clean package docker:build  -DpushImage
```

## Run docker instance

```bash
docker login --username=docker@1609893818605002 registry.cn-shanghai.aliyuncs.com
# docker run --rm -p 7829:8080 --name docker-sample docker-sample:0.0.1-SNAPSHOT
docker run -d -p 7829:8080 --cap-add=SYS_PTRACE registry.cn-shanghai.aliyuncs.com/trlgon/docker-maven-plugin
```

## View running results
&emsp;&emsp;Open a browser and type http://127.0.0.1:7829/
