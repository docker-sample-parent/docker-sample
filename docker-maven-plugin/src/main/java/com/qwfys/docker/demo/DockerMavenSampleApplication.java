package com.qwfys.docker.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 *
 * @author lwk
 * @version DockerMavenSampleApplication, v 0.1 7/29/19 8:59 AM lwk
 */
@SpringBootApplication
public class DockerMavenSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerMavenSampleApplication.class, args);
    }
}
